const express = require('express');
const config = require('./config/config');
const glob = require('glob');
const mongo = require('./config/mongo');
const modules = [];

var modulesPath = glob.sync(config.root + '/src/**/module.js');
modulesPath.forEach((modulePath) => {
  modules.push(require(modulePath));
});

modules.forEach((module) => {
  module.initModels();
});

const app = express();

module.exports = require('./config/express')(app, config, modules, mongo);

app.listen(config.port, () => {
  console.log('Express server listening on port ' + config.port);
});

