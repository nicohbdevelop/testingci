const path = require('path');
const rootPath = path.normalize(__dirname + '/..');
const env = process.env.NODE_ENV || 'development';

const config = {
  development: {
    root: rootPath,
    app: {
      name: 'test'
    },
    port: process.env.PORT || 3000,
    db: 'mongodb://admin:admin1234@cluster0-shard-00-00-p3mkn.mongodb.net:27017,cluster0-shard-00-01-p3mkn.mongodb.net:27017,cluster0-shard-00-02-p3mkn.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true&w=majority'
  },

  test: {
    root: rootPath,
    app: {
      name: 'test'
    },
    port: process.env.PORT || 3000,
    db: 'mongodb://admin:admin1234@cluster0-shard-00-00-p3mkn.mongodb.net:27017,cluster0-shard-00-01-p3mkn.mongodb.net:27017,cluster0-shard-00-02-p3mkn.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true&w=majority'
  },

  production: {
    root: rootPath,
    app: {
      name: 'test'
    },
    port: process.env.PORT || 3000,
    db: 'mongodb://admin:admin1234@cluster0-shard-00-00-p3mkn.mongodb.net:27017,cluster0-shard-00-01-p3mkn.mongodb.net:27017,cluster0-shard-00-02-p3mkn.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true&w=majority'
  }
};

module.exports = config[env];
