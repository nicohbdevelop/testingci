const max_characters = 30;

module.exports = {
    checkLength: function(phrase) {
        return phrase.length <= max_characters;
    }
}
