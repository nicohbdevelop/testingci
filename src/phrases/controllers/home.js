const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const Phrase = mongoose.model('Phrase');


exports.index = (req, res, next) => {
    Phrase.find((err, phrases) => {
        if (err) return next(err);
        console.log(phrases);
        res.render('index', {
            title: 'Best Phrases',
            phrases: phrases
        });
    });
};



