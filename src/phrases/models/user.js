// Example model

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
    id: Number,
    name: String,
    email: String,
    pass: String
});

UserSchema.virtual('date')
    .get(() => this._id.getTimestamp());

mongoose.model('User', UserSchema);
