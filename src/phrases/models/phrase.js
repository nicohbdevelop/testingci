const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PhraseSchema = new Schema({
    id: Number,
    text: String,
    author: String,
});
PhraseSchema.virtual('date')
    .get(() => this._id.getTimestamp());
mongoose.model('Phrase', PhraseSchema);
