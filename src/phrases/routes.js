module.exports = (app) => {
    const phrases = require('./controllers/home.js');

    app.get('/', phrases.index);
}
