const chai = require('chai');
const should = chai.should();
const expect = chai.expect;

const phraseSercice = require('../services/phrase-service');

describe ('This is the inspector moddule test', () => {
    describe ('Inspeccionara la longitud de la frase', () => {
        it ('Longitud correcta', () => {
            expect(phraseSercice.checkLength("Esto es una frase que mola m")).to.be.true;
        });
    });
});
