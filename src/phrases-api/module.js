const path = require('path');
const modulePath = path.normalize(__dirname + '/.');
const glob = require('glob');

module.exports = {
    initModels: function () {
        const models = glob.sync(modulePath + '/models/*.js');
        models.forEach(function (model) {
            require(model);
        });
    },

    initControllers: function (app) {
        var controllers = glob.sync(modulePath + '/controllers/*.js');
        controllers.forEach((controller) => {
            require(controller)(app);
        });
    },

    initRouting: function (app) {
        const routes = modulePath + '/routes.js';
        require(routes)(app);
    },

    initViews: function (app) {
        const views = app.get('views');
        views.push(modulePath + '/views')
    }
}
